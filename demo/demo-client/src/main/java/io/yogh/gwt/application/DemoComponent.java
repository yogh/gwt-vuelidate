package io.yogh.gwt.application;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import io.yogh.gwt.vuelidate.Validations;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

@Component(customizeOptions = DemoComponentValidator.class)
public class DemoComponent implements IsVueComponent {
  @Data String name;

  @Data String email;

  @Data boolean submitted;

  @JsProperty(name = "$v") Validations validation;

  @Computed
  public Validations getV() {
    return validation;
  }

  @JsMethod
  public void submit() {
    if (validation.isInvalid()) {
      submitted = false;
      return;
    }

    // Post
    submitted = true;
  }
}
