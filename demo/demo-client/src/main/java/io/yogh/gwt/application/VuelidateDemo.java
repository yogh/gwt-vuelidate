package io.yogh.gwt.application;

import com.axellience.vuegwt.core.client.Vue;
import com.axellience.vuegwt.core.client.VueGWT;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

import io.yogh.gwt.vuelidate.DefaultValidators;

public class VuelidateDemo implements EntryPoint {
  @Override
  public void onModuleLoad() {
    VueGWT.initWithoutVueLib();
    VueGWT.onReady(() -> {
      Vue.attach("#base", DemoComponentFactory.get());
    });
  }
}
