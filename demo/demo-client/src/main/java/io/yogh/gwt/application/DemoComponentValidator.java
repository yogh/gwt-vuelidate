package io.yogh.gwt.application;

import java.util.function.BiConsumer;

import io.yogh.gwt.vuelidate.ValidationOptions;
import io.yogh.gwt.vuelidate.ValidatorBuilder;

public class DemoComponentValidator extends ValidationOptions<DemoComponent> {
  @Override
  protected void constructValidations(final BiConsumer<String, Object> installer) {
    installer.accept("name", ValidatorBuilder.create().required().alpha());
    installer.accept("email", ValidatorBuilder.create().required().email());
  }
}
