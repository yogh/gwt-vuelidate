package io.yogh.gwt.vuelidate;

import elemental2.core.JsObject;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "vuelidate")
public class Vuelidate {
  public static JsObject validationMixin;
}
