package io.yogh.gwt.vuelidate;

import elemental2.core.JsObject;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public class Validations implements JsPropertyMap<Object> {
  @JsProperty(name = "$invalid") public boolean invalid;
  @JsProperty(name = "$dirty") public boolean dirty;
  @JsProperty(name = "$anyDirty") public boolean anyDirty;

  @JsProperty(name = "$model") public String model;

  @JsProperty(name = "$error") public boolean error;
  @JsProperty(name = "$anyError") public boolean anyError;
  @JsProperty(name = "$pending") public boolean pending;

  @JsProperty(name = "$params") public JsObject params;
  @JsProperty(name = "$each") public JsObject each;
  @JsProperty(name = "$iter") public JsObject iter;

  public native void $touch();

  public native void $reset();

  public native void $flattenParams();

  @JsOverlay
  public final boolean isInvalid() {
    return invalid;
  }

  @JsOverlay
  public final Validations find(final String key) {
    return input(key);
  }

  @JsOverlay
  public final Validations f(final String key) {
    return input(key);
  }

  @JsOverlay
  public final Validations input(final String key) {
    final String[] names = JsObject.getOwnPropertyNames(this);
    final String runtimeName = findRuntimeProperty(names, key);
    return Js.uncheckedCast(get(runtimeName));
  }

  @JsOverlay
  private static String findRuntimeProperty(final String[] properties, final String name) {
    for (int i = 0; i < properties.length; i++) {
      if (properties[i].startsWith(name + "_")) {
        return properties[i];
      }
    }

    return null;
  }
}
