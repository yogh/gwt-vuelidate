package io.yogh.gwt.vuelidate;

import elemental2.core.JsObject;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "validators")
public class DefaultValidators {
  public static JsObject alpha;
  public static JsObject alphaNum;
  public static JsObject and;
  public static JsObject between;
  public static JsObject email;
  public static JsObject decimal;
  public static JsObject helpers;
  public static JsObject integer;
  public static JsObject ipAddress;
  public static JsObject macAddress;
  public static JsObject maxLength;
  public static JsObject maxValue;
  public static JsObject minLength;
  public static JsObject minValue;
  public static JsObject not;
  public static JsObject numeric;
  public static JsObject or;
  public static JsObject required;
  public static JsObject requiredIf;
  public static JsObject requiredUnless;
  public static JsObject sameAs;
  public static JsObject url;
}
