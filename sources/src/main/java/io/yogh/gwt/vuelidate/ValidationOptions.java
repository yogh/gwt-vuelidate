package io.yogh.gwt.vuelidate;

import static elemental2.core.Global.JSON;

import java.util.function.BiConsumer;

import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.options.CustomizeOptions;
import com.axellience.vuegwt.core.client.component.options.VueComponentOptions;

import elemental2.core.JsObject;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

public abstract class ValidationOptions<T extends IsVueComponent> implements CustomizeOptions {
  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void customizeOptions(final VueComponentOptions options) {
    options.addMixin(Vuelidate.validationMixin);

    final JsPropertyMap data = Js.cast(options.get("data"));
    final Object dataFields = JSON.parse((String) data.get("dataFieldsJSON_0_1_g$"));
    final String[] properties = JsObject.getOwnPropertyNames(dataFields);

    final JsPropertyMap<Object> props = new JsPropertyMap<Object>() {};
    constructValidations((k, v) -> {
      props.set(findRuntimeProperty(properties, k), v);
    });

    options.set("validations", props);
  }

  protected abstract void constructValidations(BiConsumer<String, Object> installer);

  protected String findRuntimeProperty(final String[] properties, final String name) {
    for (int i = 0; i < properties.length; i++) {
      if (properties[i].startsWith(name + "_")) {
        return properties[i];
      }
    }

    return null;
  }
}
